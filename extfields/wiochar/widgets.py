# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
from django.forms.widgets import Widget
from django.template import Context, loader
from django.db.models.fields import NOT_PROVIDED

import  settings
from wioframework import wiotext


class WIOCharFieldWidget(Widget):

    def __init__(self, attrs={}, maxlength=None, default=None,
                 choices=None, required=False, placeholder="", fieldtype="text"):
        self.attrs = attrs
        self.choices = choices
        self.maxlength = maxlength
        self.default = default if default is not NOT_PROVIDED else None
        self.required = required
        self.placeholder = placeholder
        self.fieldtype = fieldtype

    def render(self, name, value, attrs=None):
        if value is None:
            value = self.default
        widgettemplate = loader.get_template('extfields/charfield.html')
        html_content = widgettemplate.render(Context({
            'name': name,
            'value': value,
            'MEDIA_URL': settings.MEDIA_URL,
            'maxlength': (self.maxlength * 3 if self.maxlength is not None else None),
            'appmaxlength': self.maxlength,
            'choices': self.choices,
            'required': self.required,
            'placeholder': self.placeholder,
            'rows': min(10, int(self.maxlength / 400)) if self.maxlength else 0,
            'type': self.fieldtype or 'text',
            'nolinebreak': self.fieldtype == 'url'
        }))
        return html_content

    def value_from_datadict(self, data, files, name):
        if name not in data or data[name] == None:
            return ""
        text = data[name]
        restext = wiotext.wiotext_pre(text)
        if self.fieldtype in ["url"]:
            restext = text
        # print "restext :", restext
        # import sys
        # sys.stderr.write( "restext-%s\n"%(restext,))
        return restext
